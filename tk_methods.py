#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function, division, absolute_import

import numpy as np


__all__ = ['get_radius',
           'profile',
           'get_radius_old',
           'integrated_profile']


def slow_get_radius(value, distance, profile):
    """Return the radius corresponding to the input profile value

    Parameters
    ----------
    value: float
        Profile value which the radius is wanted
    distance: `numpy.ndarray`
        Radius grid
    profile: `numpy.ndarray`
        Image profile

    Returns
    -------
    radius: float
        Distance at which the profile matches the input value
        (same dimensionality of the distance input array)

    Remarks
    -------
    Only works for a monotonious profile

    """
    out = False

    if value >= profile.max():
        out = True
        default_radius = distance.min()

    if value <= profile.min():
        out = True
        default_radius = distance.max()

    if out:
        return default_radius

    scaled_profile = profile - value
    idx = np.argmin(np.abs(scaled_profile))

    if abs(scaled_profile[idx]) > 0:
        if scaled_profile[idx] < 0:
            idmin = idx - 1
            idmax = idx
        else:
            idmin = idx
            idmax = idx + 1
        delta = (value - profile[idmin]) / (profile[idmax] - profile[idmin])
        radius = distance[idmin] + delta * (distance[idmax] - distance[idmin])
    else:
        radius = distance[idx]

    return radius


def slow_azimuthal_profile(image, origin, bin, nbins):
    """
    Returns axisymmetric profile of a 2d image.
    x, y[, n] = profile(image, [origin, bin, nbins, histogram])

    Parameters
    ----------
    input: `numpy.ndarray`
        2D input array
    origin: tuple of float
        Center of profile (Fits convention)
    bin: float
        Width of the profile bins (in unit of pixels).
    nbins: int
        Number of profile bins.

    Returns
    -------
    radius: `numpy.ndarray`
        Profile radius in unit of pixels
    profile: `numpy.ndarray`
        Profile of input array
    histo: `numpy.ndarray`
        Histogram of pixel count in each bin

    """
    nx = image.shape[0]
    ny = image.shape[1]
    xmid, ymid = origin

    rad = np.zeros(nbins, dtype=np.float)
    profile = np.zeros(nbins, dtype=np.float)
    histo = np.zeros(nbins, dtype=np.int)
    for i in range(nx):
        for j in range(ny):
            val = image[i, j]
            if np.isnan(val):
                continue
            distance = np.sqrt((i - xmid)**2 + (j - ymid)**2)
            ibin = int(distance / bin)
            if ibin >= nbins:
                continue
            rad[ibin] = rad[ibin] + distance
            profile[ibin] = profile[ibin] + val
            histo[ibin] = histo[ibin] + 1

    for i in range(nbins):
        if histo[i] != 0:
            rad[i] = rad[i] / histo[i]
            profile[i] = profile[i] / histo[i]
        else:
            rad[i] = bin * (i - 0.5)
            profile[i] = np.NaN

    return rad, profile, histo

try:
    from ._cytutils import azimuthal_profile, get_radius
except ImportError:
    azimuthal_profile = slow_azimuthal_profile
    get_radius = slow_get_radius


def profile(input, origin=None, bin=1., nbins=None, histogram=False):
    """
    Returns axisymmetric profile of a 2d image.
    x, y[, n] = profile(image, [origin, bin, nbins, histogram])

    Parameters
    ----------
    input: `numpy.ndarray`
        2D input array
    origin: tuple of float, optional (default `None` = image center)
        Center of profile (Fits convention)
    bin: float, optional (default 1)
        Width of the profile bins (in unit of pixels).
    nbins: int, optional (default `None` = adapted on origin)
        Number of profile bins.
    histogram: bool, optional (default `False`)
        If `True`, returns the histogram.

    Returns
    -------
    radius: `numpy.ndarray`
        Profile radius in unit of pixels
    profile: `numpy.ndarray`
        Profile of input array
    [histo: `numpy.ndarray`
        Histogram of pixel count in each bin]

    Note
    ----
    This method and the underlying `azimuthal_profile` function have
    been adapted from the `pysimulators` package written by P.Chanial.

    """
    input = np.ascontiguousarray(input, np.float64)
    if origin is None:
        origin = (np.array(input.shape[::-1], np.float64) - 1) / 2
    else:
        origin = np.ascontiguousarray(origin, np.float64)

    if nbins is None:
        nbins = int(max(input.shape[0] - origin[1], origin[1],
                        input.shape[1] - origin[0], origin[0]) / bin)

    x, y, n = azimuthal_profile(input, origin, bin, nbins)

    if histogram:
        return x, y, n
    else:
        return x, y


def integrated_profile(input, origin=None, bin=1., nbins=None):
    """
    Returns axisymmetric integrated profile of a 2d image.

    Parameters
    ----------
    input: numpy.ndarray
        2d input array.
    origin: tuple of int or float, optional (default `None`)
        Center of the profile. Default is the image center.
    bin: float, optional (default 1.)
        width of the profile bins (in unit of pixels).
    nbins: int, optional (default `None`)
        number of profile bins.

    Returns
    -------
    x: numpy.ndarray
        The strict upper boundary within which each integration is performed.
    y: numpy.ndarray
        The integrated profile.

    """
    x, y, n = profile(input, origin=origin, bin=bin, nbins=nbins,
                      histogram=True)
    x = np.arange(1, y.size + 1) * bin
    y[~np.isfinite(y)] = 0
    y *= n
    return x, np.cumsum(y)


def get_radius_old(value, distance, profile):
    """Return the radius corresponding to the input profile value

    Parameters
    ----------
    value: float
        Profile value which the radius is wanted
    distance: `numpy.ndarray`
        Radius grid
    profile: `numpy.ndarray`
        Image profile

    Returns
    -------
    radius: float
        Distance at which the profile matches the input value
        (same dimensionality of the distance input array)

    Remarks
    -------
    Only works for a monotonious profile

    """
    if value >= profile.max():
        radius = distance.min()
    elif value <= profile.min():
        radius = distance.max()
    else:
        wh_inside = profile <= value
        value_in = profile[wh_inside].max()
        wh_outside = profile > value
        value_out = profile[wh_outside].min()
        radius_in = distance[wh_inside][np.argmax(profile[wh_inside])]
        radius_out = distance[wh_outside][np.argmin(profile[wh_outside])]
        delta_rad = ((radius_out - radius_in) *
                     (value - value_in) /
                     (value_out - value_in))
        radius = radius_in + delta_rad

    return radius

