#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Python code to create effective PSFs #
# P. Guillard & A. Boucaud

# inputs: SEDs and PSF cubes (wavl in 3rd dimension)
# outputs: effective PSFs

from __future__ import print_function, division

import os
import sys
import glob
import time
import pyfits
import numpy as np
import scipy.interpolate as Intp
import scipy.integrate as Intg

#==============================================================================
# A MODIFIER
#------------
# -SED-
# sed_pattern = "/Users/aboucaud/work/Euclid/data/galaxy_spectra/bpz/{0}_B2004a.sed"
sed_pattern = "/Users/guillard/instruments/euclid/git/psfvardoc/data/{0}_B2004a.sed"
# -STELLAR SED-
# stellar_sed_pattern = '/Users/aboucaud/work/Euclid/data/stellar_spectra/vizier/star_{0}.sed'
stellar_sed_pattern = '/Users/guillard/instruments/euclid/git/psfvardoc/data/star_{0}.sed'
# -FILTERS-
# filter_path = "/Users/aboucaud/work/Euclid/data/filters/140401_VIS_PCE.txt"
filter_path = "/Users/guillard/instruments/euclid/VIS/VIS_bandpass/140401_VIS_PCE.txt"
# -FILE NAMES-
# psf_pattern = psf_dir + '/PSF_MC_T0072.ZMX__Npup_4096x4096_lbda_0.800um_central4ccds.cat_{:04d}.fits'
# psf_pattern = psf_dir + '/PSF_MC_T0072.ZMX__Npup_4096x4096_lbda_0.800um_bords_guillard.cat_{:04d}.fits'
psf_pattern = psf_dir + '/PSF_pdr_cool_69_v.1_NISP_NIS-blue_1024x1024_900nm_pos00{:04d}.fits' # for NISP
#==============================================================================
# Range for the files
nmin = 1
#nmax = 601
nmax = 398
# Raise error if PSF files not in current working directory
assert os.path.isfile(psf_pattern.format(1))

OUTPUTDIR = os.path.join(psf_dir, 'effective')
if not os.path.exists(OUTPUTDIR):
    os.mkdir(OUTPUTDIR)

# Check whether files have already been computed
psfdone = glob.glob(OUTPUTDIR + '/PSF_MC_T0072*.fits')
if len(psfdone) != 0:
    psfdone.sort()
    lastfile = os.path.basename(psfdone[-1])
    nmin = int(lastfile[63:67])

# psffiles = glob.glob('PSF_MC_T0072*.fits')
psffiles = [psf_pattern.format(i) for i in range(nmin, nmax)]
assert len(psffiles) != 0

psfcenter = ["PSF_MC_T0072.ZMX__Npup_4096x4096_lbda_0.800um_central4ccds.cat_0136.fits",
             "PSF_MC_T0072.ZMX__Npup_4096x4096_lbda_0.800um_central4ccds.cat_0238.fits",
             "PSF_MC_T0072.ZMX__Npup_4096x4096_lbda_0.800um_central4ccds.cat_0264.fits",
             "PSF_MC_T0072.ZMX__Npup_4096x4096_lbda_0.800um_central4ccds.cat_0288.fits",
             "PSF_MC_T0072.ZMX__Npup_4096x4096_lbda_0.800um_central4ccds.cat_0370.fits",
             "PSF_MC_T0072.ZMX__Npup_4096x4096_lbda_0.800um_central4ccds.cat_0381.fits",
             "PSF_MC_T0072.ZMX__Npup_4096x4096_lbda_0.800um_central4ccds.cat_0428.fits",
             "PSF_MC_T0072.ZMX__Npup_4096x4096_lbda_0.800um_central4ccds.cat_0509.fits",
             "PSF_MC_T0072.ZMX__Npup_4096x4096_lbda_0.800um_central4ccds.cat_0528.fits"]

psfborder = [psf for psf in psffiles if "bords" in psf]

wv_psf = [0.475, 0.48, 0.485, 0.49, 0.495, 0.5, 0.505, 0.51, 0.515, 0.52,
          0.525, 0.53, 0.535, 0.54, 0.545, 0.55, 0.555, 0.56, 0.565, 0.57,
          0.575, 0.58, 0.585, 0.59, 0.595, 0.6, 0.605, 0.61, 0.615, 0.62,
          0.625, 0.63, 0.635, 0.64, 0.645, 0.65, 0.655, 0.66, 0.665, 0.67,
          0.675, 0.68, 0.685, 0.69, 0.695, 0.7, 0.705, 0.71, 0.715, 0.72,
          0.725, 0.73, 0.735, 0.74, 0.745, 0.75, 0.755, 0.76, 0.765, 0.77,
          0.775, 0.78, 0.785, 0.79, 0.795, 0.8, 0.805, 0.81, 0.815, 0.82,
          0.825, 0.83, 0.835, 0.84, 0.845, 0.85, 0.855, 0.86, 0.865, 0.87,
          0.875, 0.88, 0.885, 0.89, 0.895, 0.9, 0.905, 0.91, 0.915, 0.92,
          0.925, 0.93, 0.935, 0.94, 0.945, 0.95, 0.955, 0.96, 0.965, 0.97]

wv_filter, tr_filter = np.loadtxt(filter_path, usecols=[0,1], unpack=True)

def load_vis_cube(filename):
    psf_cube = np.zeros((len(wv_psf), 512, 512), dtype=float)
    with pyfits.open(filename) as fitsfile:
        for i_wv, wv in enumerate(wv_psf):
            psf_cube[i_wv] = fitsfile[i_wv + 1].data
        header = fitsfile[1].header
    return psf_cube, header

def simple_average(filename):
    psf, hdr = load_vis_cube(filename)
    outpsf = psf.mean(axis=0)
    hdr['WLGTH0'] = np.mean(wv_psf)
    pyfits.writeto('effective/{}'.format(filename),
                   data=outpsf,
                   header=hdr)

class EffectivePSF(object):
    """
    """
    def __init__(self, filename, source):
        self.filename = filename
        self.source = source

        self.psf = 0.0
        self.psf_cube, self.header = load_vis_cube(self.filename)

        self._initialized_norm = False
        self._initialized_templates = False
        self._initialized_computation = False

    def load_templates(self):
        self.filter_temp = wv_filter, tr_filter
        self.source_temp = self._load_source_spectrum(self.source)
        self.wv_min = wv_psf[0]
        self.wv_max = wv_psf[-1]
        self.wv_range = np.linspace(self.wv_min, self.wv_max, 500)

        self._initialized_templates = True

    @staticmethod
    def _load_source_spectrum(source):
        if source == 'flat':
            return None
        elif source in ['ell', 'spb', 'irr']:
            # wavelength given in Angstroms
            wv_sed, sed = np.loadtxt(sed_pattern.format(source), unpack=True)
            return (wv_sed * 1.e-4, sed)
        elif source in ['o5v', 'm5v']:
            # wavelength given in nanometers
            wv_sed, sed = np.loadtxt(stellar_sed_pattern.format(source),
                                     unpack=True)
            return (wv_sed * 1.e-3, sed)
        else:
            raise AttributeError('Unknown source type for spectrum')

    def instrumental_thruput(self, wavelength):
        """
        Return instrumental throughput at the provided wavelength values

        Computes a linear interpolation around the design values of the
        filters + optics and corrects for the quantum efficiency

        Parameters
        ----------
        wv_range: 1D `numpy.ndarray`
            Wavelength range in micrometers

        """
        if not self._initialized_templates:
            self.load_templates()

        return np.interp(wavelength, *self.filter_temp)
        # return np.ones_like(wavelength)

    def source_spectrum(self, wavelength, interp_method='linear'):
        """
        Return the spectrum of the source at the provided wavelength values

        Parameters
        ----------
        wv_range: 1D `numpy.ndarray`
            Wavelength range in micrometers

        interp_method: str, optional
            Interpolation method desired
                * 'linear' (default)
                * 'spline'

        """
        if not self._initialized_templates:
            self.load_templates()

        if not self.source_temp:
            return np.ones_like(wavelength)
        else:
            # From Angstroms to micrometers
            if interp_method == 'linear':
                return np.interp(wavelength, *self.source_temp)
            elif interp_method == 'spline':
                spl = Intp.InterpolatedUnivariateSpline(*self.source_temp)
                return spl(wavelength)
            else:
                print("Not implemented yet")
                pass

    def _init_pixel_response_spline(self, irow, icol):
        if not self._initialized_templates:
            self.load_templates()

        ln_pixel = np.log(self.psf_cube[:, irow, icol])
        self.pr_ln_spline = Intp.InterpolatedUnivariateSpline(wv_psf,
                                                              ln_pixel)

    def pixel_response(self, wavelength):
        return np.exp(self.pr_ln_spline(wavelength))

    def photon_count(self, wavelength):
        return (self.instrumental_thruput(wavelength) *
                self.source_spectrum(wavelength) /
                wavelength)

    def _integrand(self, wavelength):
        return (self.photon_count(wavelength) *
                self.pixel_response(wavelength))

    def init_normalization_factor(self):
        # invnorm, _ = Intg.quad(
            # self.photon_count, self.wv_min, self.wv_max,
            # limit=200)
        dwv = self.wv_range[1] - self.wv_range[0]
        invnorm = np.sum(self.photon_count(self.wv_range)) * dwv
        self.norm = 1 / invnorm

        self._initialized_norm = True

    def effective_pixel_response(self, irow, icol):
        if not self._initialized_norm:
            self.init_normalization_factor()

        self._init_pixel_response_spline(irow, icol)

        integral, _ = Intg.quad(
            self._integrand, self.wv_min, self.wv_max,
            limit=200)

        return integral * self.norm

    def poor_effective_pixel_response(self, irow, icol):
        if not self._initialized_norm:
            self.init_normalization_factor()

        self._init_pixel_response_spline(irow, icol)

        dwv = self.wv_range[1] - self.wv_range[0]
        integral = np.sum(self._integrand(self.wv_range)) * dwv

        return integral * self.norm

    def run(self, save=False):
        if not self._initialized_templates:
            self.load_templates()

        self.init_normalization_factor()

        # counter = "Computing pixel {0}/{1} ({2:d}%)"
        # print("\n")

        _, nrows, ncols = self.psf_cube.shape
        ntot = nrows * ncols
        self._psf = np.zeros((nrows, ncols), dtype=float)
        start_time = time.time()
        for irow in range(nrows):
            for icol in range(ncols):
                # print('\r', end='')
                # i_pix = irow*ncols + icol + 1
                # sys.stdout.write(counter.format(i_pix, ntot,
                #                                 int(i_pix*100 / ntot)))
                # sys.stdout.flush()

                self._psf[irow, icol] = self.poor_effective_pixel_response(irow, icol)

        # dtime = int(time.time() - start_time)
        # print("\nElapsed time: {0:02d}h{1:02d}m{2:02d}s\n".format(
        #     dtime // 3600, (dtime % 3600) // 60 , dtime % 60))
        # print("Euclid effective PSF computed for",
        #       "file {}".format(self.filename),
        #       "and source {}".format(self.source),
        #       sep="\n\t")

        self._initialized_computation = True

        if save:
            self.write_effective_psf(save, self.header)


    @property
    def psf(self):
        if not self._initialized_computation:
            self.run()

        return self._psf

    @psf.setter
    def psf(self, value):
        self._psf = value

    def write_effective_psf(self, outfits, fitshdr=None):
        if fitshdr is not None:
            fitshdr.add_comment("==========================================")
            fitshdr.add_comment("Effective PSF created from 100 wavelengths")
            fitshdr.add_comment("==========================================")
        if not os.path.isfile(outfits):
            fits = pyfits.PrimaryHDU(data=self.psf, header=fitshdr)
            fits.writeto(outfits, clobber=True)
            print("Output image written in {}".format(outfits))
        else:
            print("Output file {} already existing".format(outfits))


def create_vis_eff_psf(filename, sed):
    # for sed in ['ell', 'irr', 'spb', 'flat']:
    outfile = filename.replace('.fits', '_effective_{}.fits'.format(sed))
    #outpath = '{}/{}'.format(OUTPUTDIR, outfile)
    outpath = os.path.join(OUTPUTDIR, os.path.basename(outfile))
    # if os.path.isfile(outpath):
    #     continue
    # else:
    new_psf = EffectivePSF(filename, sed)
    new_psf.run(save=outpath)


def main():
    for psf in psffiles:
        print(psf)
        # simple_average(psf)
        for sed in ['ell', 'irr', 'spb', 'flat']:
            create_vis_eff_psf(psf, sed)


def main_mp():
    import multiprocessing as mp
    pool = mp.Pool(processes=4)
    for psf in psffiles:
        res = [pool.apply_async(create_vis_eff_psf, args=(psf, sed,))
               for sed in ['ell', 'irr', 'spb', 'flat']]
        for p in res:
            p.get()


def main_stars():
    psflis = psfcenter + psfborder
    for psf in psflis:
        for sed in ["o5v", "m5v"]:
            create_vis_eff_psf(psf, sed)


if __name__ == '__main__':
    main_stars()
