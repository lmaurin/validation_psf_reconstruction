#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
PSF fitting tools

Version:
  1.0

Revision:
  June 10 2015 P. Guillard
  TO BE REVISED FOR MIRI PSF

"""

from __future__ import division, print_function

import os
import astropy.io.fits as pyfits
import numpy as np

from tk_methods import profile, integrated_profile, get_radius

from scipy.special import jn
from scipy.optimize import curve_fit, leastsq


# Convenience functions
# ---------------------

def sigma2fwhm(sigma):
    fwhm = 2*np.sqrt(2*np.log(2)) * sigma
    return fwhm

def fwhm2sigma(fwhm):
    return fwhm / (2*np.sqrt(2*np.log(2)))


# Main class
# ----------

class PSFfit(object):
    """Base class for fitting models to PSF data"""
    def __init__(self, data, rotation=False):
        self.data = np.asarray(data, dtype=float)
        self.rotation = rotation

        self.dims = self.data.ndim

        if self.dims == 1:
            self.coords = np.arange(len(self.data))
        elif self.dims == 2:
            self.coords = np.indices(self.data.shape)
        elif self.dims == 3:
            raise NotImplementedError()
        else:
            ValueError("The dimension of the data is too big")

        if self.data.size == 0:
            raise ValueError("Data is empty")
        elif self.data.size > 100000:
            self._reduce_data_size()
        else:
            self._slide = (0.0, 0.0)
            self._data_backup = np.array([[]])

        self._params = self.moments
        self._is_optimized = False

    @property
    def params(self):
        return self._params

    @property
    def optimized(self):
        return self._is_optimized

    @property
    def bigarray(self):
        return self._data_backup.size != 0

    @property
    def moments(self):
        """Compute a first guess for the distribution parameters

        Uses the first moments of the data for position and width and
        sets the angle to 0.

        """
        # Constant
        const = self.data.min()
        # Remove baseline
        bdata = self.data - const
        # Retrieve scale
        scale = bdata.max()
        # Compute moments
        total = bdata.sum()

        if self.dims == 1:
            mu_x = np.sum(self.coords * bdata) / total
            mu_y = 0.0
            sigma_x = np.sqrt(np.abs(
                np.sum((self.coords-mu_x)**2 * bdata) / total))
            sigma_y = 0.0
        else:
            x, y = self.coords
            # First moments
            mu_x = int(round(np.sum(x * bdata) / total))
            mu_y = int(round(np.sum(y * bdata) / total))
            
            col = bdata[:, mu_y]
            row = bdata[mu_x, :]

            # Second moments
            sigma_x = np.sqrt(np.abs(
                np.sum((np.arange(col.size)-mu_y)**2 * col) / np.sum(col)))
            sigma_y = np.sqrt(np.abs(
                np.sum((np.arange(row.size)-mu_x)**2 * row) / np.sum(row)))

        return (scale, mu_x, mu_y, sigma_x, sigma_y, const, 0.0)

    def distribution(self, params):
        return np.ones_like(self.data)

    def _reduce_data_size(self, size=300):
        orig_data = self.data
        orig_shape = self.data.shape
        _, x_guess, y_guess, _, _, _, _ = self.moments
        xmin = np.max([int(x_guess) - int(size/2), 0])
        xmax = np.min([xmin + size, orig_shape[0] - 1])
        if self.dims == 2:
            ymin = np.max([int(y_guess) - int(size/2), 0])
            ymax = np.min([ymin + size, orig_shape[1] - 1])
            self.data = orig_data[xmin:xmax, ymin:ymax]
            self.coords = np.indices(self.data.shape)
            self._slide = (xmin, ymin)
        else:
            self.data = orig_data[xmin:xmax]
            self.coords = np.arange(self.data.size)
            self._slide[0] = xmin

        self._data_backup = orig_data.copy()

    def run_fit(self):
        error_func = lambda p: np.ravel(
            self.distribution(p) - self.data)

        pfit, success = leastsq(error_func, self.params)

        if not success:
            raise ValueError("The fitting procedure did not succeed")

        self._params = pfit
        self._is_optimized = True

    def fitted_data(self):
        if not self.optimized:
            self.run_fit()

        return self.distribution(self.params)

    def print_info(self):
        if not self.optimized:
            self.run_fit()

        scale, mu_x, mu_y, sigma_x, sigma_y, const, theta = self.params
        
        print("\n{}D {} fit results:".format(self.dims, self.FIT))
        print("-----------------------")
        print("Background level = {:.2f}".format(const))
        if self.dims == 1:
            print("center = ({:1.1f})".format(self.get_center()))
            print("FWHM = {:1.2f} pixels (sigma = {:1.2f})".format(
                    sigma2fwhm(sigma_x), sigma_x))
        else:
            print("center = ({:1.1f}, {:1.1f})".format(*self.get_center()))
            print("FWHM x = {:1.2f} pixels (sigma_x = {:1.2f})".format(
                    sigma2fwhm(sigma_x), sigma_x))
            print("FWHM y = {:1.2f} pixels (sigma_y = {:1.2f})".format(
                    sigma2fwhm(sigma_y), sigma_y))
            if self.rotation:
                # Due to symmetries, the rotation angle should be < 180 deg
                norm_angle = np.mod(theta, 180)
                print("\ttheta = {:1.2f} deg".format(norm_angle))

    def get_center(self):
        if not self.optimized:
            self.run_fit()

        # Add zero point if any
        add_x, add_y = self._slide

        if self.dims == 1:
            return self.params[1] + add_x
        else:
            return (self.params[1] + add_x, self.params[2] + add_y)

    def get_fwhm(self):
        if self.dims == 1:
            return -1
        else:
            return (-1, -1)

    def get_angle(self):
        if self.dims == 1:
            raise ValueError("Cannot compute the rotation on 1D data")
        if not self.rotation:
            raise AttributeError("Rotation not allowed on __init__")

        if not self.optimized:
            self.run_fit()

        return self.params[-1]

    def get_eccentricity(self):
        if self.dims == 1:
            return 0
        else:
            b, a = self.get_fwhm()

            if a == 0.0:
                return 0

            return np.sqrt(1 - (b / a)**2)

    def get_averaged_fwhm(self):
        if self.bigarray:
            image = self._data_backup
        else:
            image = self.data

        dist, prof = profile(image, origin=self.get_center())
        hwhm = get_radius(0.5*prof.max(), dist, prof)

        return 2 * hwhm

    def get_r65(self, rad_norm=None):
        if self.bigarray:
            image = self._data_backup
        else:
            image = self.data

        dist, iprof = integrated_profile(image, origin=self.get_center())

        if rad_norm:
            norm_val = get_radius(rad_norm, iprof, dist)
            return get_radius(0.65*norm_val, dist, iprof)

        return get_radius(0.65*iprof.max(), dist, iprof)




# Sub-classes
# -----------

class GaussianPSFfit(PSFfit):
    FIT = "Gaussian"
    """Sub class of PSFfit dealing with Gaussian distributions"""
    def __init__(self, data, **kwargs):
        PSFfit.__init__(self, data, **kwargs)

    def distribution(self, params):
        # Unwrap parameters
        scale, mu_x, mu_y, sigma_x, sigma_y, const, theta = params

        if self.dims == 1:
            return (scale * np.exp(-0.5 * ((self.coords-mu_x) / sigma_x)**2)
                    + const)
        else:
            x, y = self.coords
            if self.rotation:
                # Convert angle to radians
                rtheta = np.deg2rad(theta)
                xrot = (x-mu_x) * np.cos(rtheta) - (y-mu_y) * np.sin(rtheta)
                yrot = (x-mu_x) * np.sin(rtheta) + (y-mu_y) * np.cos(rtheta)
                return (scale * np.exp(-0.5 * ((xrot / sigma_x)**2 +
                                               (yrot / sigma_y)**2))
                        + const)
            else:
                return (scale * np.exp(-0.5 * (((x-mu_x) / sigma_x)**2 +
                                               ((y-mu_y) / sigma_y)**2))
                        + const)

    def get_fwhm(self):
        if not self.optimized:
            self.run_fit()

        if self.dims == 1:
            return sigma2fwhm(self.params[3])
        else:
            return np.sort(map(sigma2fwhm, self.params[3:5]))


class AiryPSFfit(PSFfit):
    FIT = "Airy"
    """Sub class of PSFfit dealing with Airy distributions"""
    def __init__(self, data, **kwargs):
        PSFfit.__init__(self, data, **kwargs)

    def distribution(self, params):
        # Unwrap parameters
        scale, mu_x, mu_y, fwhm_x, fwhm_y, const, theta = params

        if self.dims == 1:
            dist = np.abs(self.coords - mu_x)
            dist[dist==0] = 1.e-30
            dist *= 1.61633 / (fwhm_x / 2)
        else:
            x, y = self.coords
            if self.rotation:
                # Convert angle to radians
                rtheta = np.deg2rad(theta)
                coord_x = (x-mu_x) * np.cos(rtheta) - (y-mu_y) * np.sin(rtheta)
                coord_y = (x-mu_x) * np.sin(rtheta) + (y-mu_y) * np.cos(rtheta)
            else:
                coord_x = np.abs(x - mu_x)
                coord_y = np.abs(y - mu_y)

            coord_x[coord_x==0] = 1.e-30
            coord_x *= 1.61633 / (fwhm_x / 2)
            coord_y[coord_y==0] = 1.e-30
            coord_y *= 1.61633 / (fwhm_y / 2)

            dist = np.hypot(coord_x, coord_y)

        airy = (2 * jn(1, dist) / dist)**2

        return scale * airy + const

    def get_fwhm(self):
        if not self.optimized:
            self.run_fit()

        if self.dims == 1:
            return self.params[3]
        else:
            return np.sort(self.params[3:5])


def infoPSF(cls, image, psc=None, rotation=True):
    info = {}
    if psc:
        info['scale'] = 'arcsec'
        mult = psc
    else:
        info['scale'] = 'pixel'
        mult = 1.0
    fit2d = cls(image, rotation=rotation)
    info['center'] = fit2d.get_center()
    info['fwhm2d'] = fit2d.get_fwhm() * mult
    info['fwhmavg'] = fit2d.get_averaged_fwhm() * mult
    info['r65'] = fit2d.get_r65() * mult
    if rotation:
        info['angle'] = fit2d.get_angle()
        info['eccentricity'] = fit2d.get_eccentricity()
    # 1D profiles
    xmax, ymax = map(round, fit2d.get_center())
    im1 = image[xmax, :]
    im2 = image[:, ymax]
    for im, axis in zip([im1, im2], ['fwhmx', 'fwhmy']):
        fit = cls(im)
        info[axis] = fit.get_fwhm() * mult

    return info


def writeInfoPSF(filename, profil='gauss', outpath='.'):
    image, hdr = pyfits.getdata(filename, header=True)
    if hdr.get('CD1_1'):
        psc = hdr['CD1_1'] * 3600
    else:
        psc = None
    basename = os.path.basename(filename)
    strname = os.path.splitext(basename)[0]
    outtext = (
        """filename\t{fname}
scale   \t{scale}
center_x\t{center[0]:1.3f}
center_y\t{center[1]:1.3f}
fwhm_x  \t{fwhmx:1.3f}
fwhm_y  \t{fwhmy:1.3f}
fwhm_min\t{fwhm2d[0]:1.3f}
fwhm_max\t{fwhm2d[1]:1.3f}
fwhm_avg\t{fwhmavg:1.3f}
r_65    \t{r65:1.3f}
angle   \t{angle:1.3f}
eccent  \t{eccentricity:1.3f}""")

    if profil.lower() == 'gaussian':
        clas = GaussianPSFfit
    elif profil.lower() == 'airy':
        clas = AiryPSFfit
    else:
        raise NotImplementedError()
    header = "# Results of the {} fit:\n".format(profil.capitalize)
    outname = '{}/{}_{}.txt'.format(outpath, strname, profil.lower())
    with open(outname, 'w') as outfile:
        dico = infoPSF(clas, image, psc=psc, rotation=True)
        outfile.write(header)
        outfile.write(outtext.format(fname=basename, **dico))
    print("{} fit DONE !".format(profil.capitalize()))


def to_latex(a, label='A \\oplus B'):
    import sys
    sys.stdout.write('\[ '
                 + label
                 + ' = \\left| \\begin{array}{'
                 + ('c'*a.shape[1])
                 + '}\n' )
    for r in a:
        sys.stdout.write(str(r[0]))
        for c in r[1:]:
            sys.stdout.write(' & '+str(c))
        sys.stdout.write('\\\\\n')
    sys.stdout.write('\\end{array} \\right| \]\n')


def main():
    import sys
    filename = sys.argv[1]
    for prof in ['gaussian', 'airy']:
        writeInfoPSF(filename, profil=prof)

if __name__ == '__main__':
    main()
